<?php

namespace Magenest\Movie\Controller\Index;

use Magenest\Movie\Model\MovieFactory;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends Action
{
    protected $_pageFactory;

    protected $_movieFactory;

    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        MovieFactory $movieFactory
    )
    {
        $this->_pageFactory = $pageFactory;
        $this->_movieFactory = $movieFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        $this->_view->loadLayout();
//        $this->_view->getLayout()->initMessages();
        $this->_view->renderLayout();
    }
}