<?php

namespace Magenest\Movie\Controller\Adminhtml\Movie;

use Exception;
use Magenest\Movie\Model\MovieFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Save extends Action
{

    const ADMIN_RESOURCE = 'movie';
    protected $resultPageFactory;
    protected $tempFactory;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        MovieFactory $movieFactory
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->tempFactory = $movieFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();

        if ($data) {
            try {
                $id = $data['movie_id'];

                $movie = $this->tempFactory->create()->load($id);

                $data = array_filter($data, function ($value) {
                    return $value !== '';
                });


                $movie->setData($data);
                $movie->save();
                $this->_eventManager->dispatch(
                    'movie_save_before',
                    ['movie' => $movie, 'request' => $this->getRequest()]
                );
                $this->messageManager->addSuccess(__('Successfully saved the item.'));

                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                return $resultRedirect->setPath('*/*/index');
            } catch (Exception $e) {
                $this->messageManager->addError($e->getMessage());
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData($data);
                return $resultRedirect->setPath('*/*/edit', ['id' => $movie->getId()]);
            }
        }

        return $resultRedirect->setPath('*/*/index');
    }
}