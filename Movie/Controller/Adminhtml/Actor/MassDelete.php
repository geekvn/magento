<?php

namespace Magenest\Movie\Controller\Adminhtml\Actor;

use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Ui\Component\MassAction\Filter;
use Magenest\Movie\Model\ResourceModel\Actor\DataFactory;

/**
 * Class MassDelete
 * @package Mageplaza\Blog\Controller\Adminhtml\Post
 */
class MassDelete extends Action
{
    /**
     * Mass Action Filter
     *
     * @var Filter
     */
    public $filter;
    /**
     * Collection Factory
     *
     * @var DataFactory
     */
    public $dataFactory;

    /**
     * constructor
     *
     * @param Filter $filter
     * @param DataFactory $dataFactory
     * @param Context $context
     */
    public function __construct(
        Context $context,
        Filter $filter,
        DataFactory $dataFactory
    )
    {
        $this->filter = $filter;
        $this->dataFactory = $dataFactory;
        parent::__construct($context);
    }

    /**
     * @return $this|ResponseInterface|ResultInterface
     * @throws LocalizedException
     */
    public function execute()
    {
        $collection = $this->filter->getCollection($this->dataFactory->create());
        try {
            $collection->walk('delete');
            $this->messageManager->addSuccessMessage(__('Actor has been deleted.'));
        } catch (Exception $e) {
            $this->messageManager->addErrorMessage(__($e->getMessage()));
        }
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }
}