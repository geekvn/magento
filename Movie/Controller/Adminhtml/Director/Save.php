<?php

namespace Magenest\Movie\Controller\Adminhtml\Director;

use Exception;
use Magenest\Movie\Model\DirectorFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Save extends Action
{

    const ADMIN_RESOURCE = 'director';

    protected $resultPageFactory;
    protected $tempFactory;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        DirectorFactory $directorFactory
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->tempFactory = $directorFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();

        if ($data) {
            try {
                $id = $data['director_id'];

                $director = $this->tempFactory->create()->load($id);

                $data = array_filter($data, function ($value) {
                    return $value !== '';
                });

                $director->setData($data);
                $director->save();
                $this->messageManager->addSuccess(__('Successfully saved the item.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                return $resultRedirect->setPath('*/*/');
            } catch (Exception $e) {
                $this->messageManager->addError($e->getMessage());
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData($data);
                return $resultRedirect->setPath('*/*/edit', ['id' => $director->getId()]);
            }
        }

        return $resultRedirect->setPath('*/*/index');
    }
}