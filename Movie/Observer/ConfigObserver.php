<?php

namespace Magenest\Movie\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;

class ConfigObserver implements ObserverInterface
{
    const XML_PATH_TEXT_FIELD_URL = 'movie/general/text_field';

    /**
     * @var RequestInterface
     */
    private $request;
    private $configWriter;
    /**
     * ConfigChange constructor.
     * @param RequestInterface $request
     * @param WriterInterface $configWriter
     */
    public function __construct(
        RequestInterface $request,
        WriterInterface $configWriter
    ) {
        $this->request = $request;
        $this->configWriter = $configWriter;

    }

    public function execute(Observer $observer)
    {
        $textParams = $this->request->getParam('groups');
        $textVal = $textParams['general']['fields']['text_field']['value'];
        if (strtolower($textVal) == 'ping') $textVal = 'Pong';

        $this->configWriter->save('movie/general/text_field', $textVal);
        return $this;
    }
}