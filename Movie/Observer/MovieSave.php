<?php

namespace Magenest\Movie\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class MovieSave implements ObserverInterface
{

    public function execute(Observer $observer)
    {
        $movie = $observer->getMovie();
        $movie->setRating(0)->save();
    }
}