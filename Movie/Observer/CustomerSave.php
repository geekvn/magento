<?php

namespace Magenest\Movie\Observer;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class CustomerSave implements ObserverInterface
{
    protected $_customerRepositoryInterface;

public function __construct(CustomerRepositoryInterface $customerRepositoryInterface)
{
    $this->_customerRepositoryInterface = $customerRepositoryInterface;
}

    public function execute(Observer $observer)
    {
        // TODO: Implement execute() method.
        $customer = $observer->getEvent()->getCustomer();
        $customer->setPrefix("Magenest");
        $this->_customerRepositoryInterface->save($customer);
    }
}