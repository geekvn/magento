<?php

namespace Magenest\Movie\Model;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;

class Actor extends AbstractModel
{

    public function __construct(
        Context $context,
        Registry $registry,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        ResourceConnection $resourceConnection,
        array $data = []
    )
    {
        $this->resourceConnection = $resourceConnection;
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection,
            $data
        );
    }

    public function _construct()
    {
        $this->_init('Magenest\Movie\Model\ResourceModel\Actor');
    }

    public function CountActor()
    {
        $connection = $this->resourceConnection->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $results = $connection->select()->from(
            'magenest_actor',
            ['actor_id']
        );
        return count($this->resourceConnection->getConnection()->fetchCol($results));
    }
}