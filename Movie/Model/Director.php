<?php

namespace Magenest\Movie\Model;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;

class Director extends AbstractModel
{

    public function __construct(
        Context $context,
        Registry $registry,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        ResourceConnection $resourceConnection,
        array $data = []
    )
    {
        $this->resourceConnection = $resourceConnection;
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection,
            $data
        );
    }

    public function _construct()
    {
        $this->_init('Magenest\Movie\Model\ResourceModel\Director');
    }

    public function CountDirector()
    {
        $connection = $this->resourceConnection->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $results = $connection->select()->from(
            'magenest_director',
            ['director_id']
        );
        return count($this->resourceConnection->getConnection()->fetchCol($results));
    }
}