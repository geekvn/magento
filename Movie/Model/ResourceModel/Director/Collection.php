<?php

namespace Magenest\Movie\Model\ResourceModel\Director;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Subscription Collection
 */
class Collection extends AbstractCollection
{
    /**
     * Initialize resource collection
     *
     * @return void
     */
    protected $_idFieldName = 'director_id';

    public function _construct()
    {
        $this->_init('Magenest\Movie\Model\Director', 'Magenest\Movie\Model\ResourceModel\Director');
    }

    protected function _initSelect()
    {
        parent::_initSelect();

        $this->getSelect()->order('director_id');
        return $this;
    }
    public function toOptionArray()
    {
        $options = [];
        $propertyMap = [
            'value' => 'director_id',
        ];

        foreach ($this as $item) {
            $option = [];
            foreach ($propertyMap as $code => $field) {
                $option[$code] = $item->getData($field);
            }
            $option['label'] = $item->getID(). ' - ' . $item->getName();
            $options[] = $option;
        }
        if (count($options) > 0) {
            array_unshift(
                $options,
                ['value' => '', 'label' => __('Please select a director')]
            );
        }
        return $options;
    }
}