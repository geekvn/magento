<?php

namespace Magenest\Movie\Model\ResourceModel\Movie;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Subscription Collection
 */
class Collection extends AbstractCollection
{
    /**
     * Initialize resource collection
     *
     * @return void
     */
    protected $_idFieldName = 'movie_id';

    public function _construct()
    {
        $this->_init('Magenest\Movie\Model\Movie', 'Magenest\Movie\Model\ResourceModel\Movie');
    }

    protected function _initSelect()
    {
        $this->addFilterToMap('movie_id', 'main_table.movie_id');

        parent::_initSelect();

        $this->getSelect()->joinLeft(
            ['secondTable' => $this->getTable('magenest_director')],
            'main_table.director_id = secondTable.director_id',
            ['director_name' => 'name']
        )->joinLeft(
            ['three' => $this->getTable('magenest_movie_actor')],
            'main_table.movie_id = three.movie_id',
            ['actor_id']
        )->joinLeft(
            ['four' => $this->getTable('magenest_actor')],
            'three.actor_id = four.actor_id',
            ['actor_name' => 'name']
        )->order('main_table.movie_id');
        return $this;
    }
}
