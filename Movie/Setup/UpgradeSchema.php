<?php

namespace Magenest\Movie\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        if(version_compare($context->getVersion(),'0.0.1') < 0 ){
            // TODO: Implement upgrade() method.
//Table Magenest Director
            $table = $setup->getConnection()
                ->newTable($setup->getTable('magenest_director'))
                ->addColumn(
                    'director_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'auto_increment' => true,
                        'identity' => true,
                        'nullable' => false,
                        'primary' => true,
                        'unsigned' => true,
                    ],
                    'Director ID'
                )
                ->addColumn(
                    'name',
                    Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => false,
                    ],
                    'Name'
                )->setComment("Magenest_Director");
            $setup->getConnection()->createTable($table);

            $table1 = $setup->getConnection()
                ->newTable($setup->getTable('magenest_actor'))
                ->addColumn(
                    'actor_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'nullable' => false,
                        'primary' => true,
                        'unsigned' => true,
                    ],
                    'Actor ID'
                )
                ->addColumn(
                    'name',
                    Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => false,
                    ],
                    'Name'
                )->setComment("Magenest_Actor");
            $setup->getConnection()->createTable($table1);
//Table Magenest Movie
            $table2 = $setup->getConnection()
                ->newTable($setup->getTable('magenest_movie'))
                ->addColumn(
                    'movie_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'nullable' => false,
                        'primary' => true,
                        'unsigned' => true,
                    ],
                    'Director ID'
                )
                ->addColumn(
                    'name',
                    Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => false,
                    ],
                    'Name'
                )
                ->addColumn(
                    'description',
                    Table::TYPE_TEXT,
                    '255',
                    [
                        'nullable' => false,
                    ],
                    'Description'
                )
                ->addColumn(
                    'rating',
                    Table::TYPE_INTEGER,
                    2,
                    [
                        'nullable' => false,
                        'unsigned' => true,
                    ],
                    'Rating'
                )
                ->addColumn(
                    'director_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'nullable' => true,
                        'unsigned' => true,
                    ],
                    'Director ID'
                )
                ->addForeignKey(
                    $setup->getFkName(
                        'magenest_movie',
                        'movie_id',
                        'magenest_director',
                        'director_id'
                    ),
                    'director_id',
                    $setup->getTable('magenest_director'),
                    'director_id',
                    Table::ACTION_CASCADE
                )
                ->setComment("Magenest_Movie");
            $setup->getConnection()->createTable($table2);
//Table Magenest Actor
            $table3 = $setup->getConnection()
                ->newTable($setup->getTable('magenest_movie_actor'))
                ->addColumn(
                    'movie_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'nullable' => false,
                        'unsigned' => true,
                    ],
                    'Movie ID'
                )
                ->addColumn(
                    'actor_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'nullable' => false,
                        'unsigned' => true,
                    ],
                    'Actor ID'
                )
                ->addForeignKey(
                    $setup->getFkName(
                        'magenest_movie_actor',
                        'movie_id',
                        'magenest_movie',
                        'movie_id'
                    ),
                    'movie_id',
                    $setup->getTable('magenest_movie'),
                    'movie_id',
                    Table::ACTION_CASCADE
                )
                ->addForeignKey(
                    $setup->getFkName(
                        'magenest_movie_actor',
                        'actor_id',
                        'magenest_actor',
                        'actor_id'
                    ),
                    'actor_id',
                    $setup->getTable('magenest_actor'),
                    'actor_id',
                    Table::ACTION_CASCADE
                )->setComment("Magenest_Movie_Actor");
            $setup->getConnection()->createTable($table3);

            $setup->endSetup();
        }

    }
}