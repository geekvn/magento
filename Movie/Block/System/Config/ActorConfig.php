<?php

namespace Magenest\Movie\Block\System\Config;

use Magenest\Movie\Model\Actor;
use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;


class ActorConfig extends Field
{
    public function __construct(
        Context $context,
        Actor $actor,
        array $data = []
    )
    {
        $this->actor = $actor;
        parent::__construct($context, $data);
    }

    protected function _getElementHtml(AbstractElement $element)
    {
        $element->setReadonly('readonly');
        $element->setValue($this->actor->CountActor());
        return $element->getElementHtml();
    }
}
