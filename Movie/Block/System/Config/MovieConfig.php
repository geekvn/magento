<?php

namespace Magenest\Movie\Block\System\Config;

use Magenest\Movie\Model\Movie;
use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;

class MovieConfig extends Field
{
    public function __construct(
        Context $context,
        Movie $movie,
        array $data = []
    )
    {
        $this->movie = $movie;
        parent::__construct($context, $data);
    }

    protected function _getElementHtml(AbstractElement $element)
    {
        $element->setReadonly('readonly');
        $element->setValue($this->movie->CountMovie());
        return $element->getElementHtml();

    }

}