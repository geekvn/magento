<?php

namespace Magenest\Movie\Block\System\Config;


use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;

class Button extends Field
{

    /**
     * Path to block template
     */
    const CHECK_TEMPLATE = 'system/config/button.phtml';

    public function __construct(Context $context,
                                $data = array())
    {
        parent::__construct($context, $data);
    }

    public function render(AbstractElement $element)
    {
// Remove scope label
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();
        return parent::render($element);
    }

    /**
     * Set template to itself
     *
     * @return $this
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (!$this->getTemplate()) {
            $this->setTemplate(static::CHECK_TEMPLATE);
        }
        return $this;
    }

    protected function _getElementHtml(AbstractElement $element)
    {
        $this->addData(
            [
                'html_id' => $element->getHtmlId()
            ]
        );

        return $this->_toHtml();
    }

}